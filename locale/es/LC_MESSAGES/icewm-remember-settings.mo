��          �      ,      �  6   �      �  �   �     �     �     �  T   �  w   B     �     �     �  U   �     )  <   /  ;   l  	   �  �  �  E   ?  >   �  W  �  
        '     -  j   2  �   �     /	     ;	     L	  g   Q	     �	  A   �	  I   
     K
                                  	                
                            <b>Select a program. Store it's window properties.</b> Add/Remove IceWM Window Defaults Entries shown below are for the <b>$appclass</b> ($appname) window.\n\nAll options marked will be saved, all unmarked will be deleted.\n\n Note: Workspace number shown is the window's current workspace. \n 	Don't worry that it appears too low.\n\n Geometry HELP Layer Next time you launch the program, it will remember the window properties last saved. Save the <b>size and position</b>, <b>workspace</b> and <b>layer</b> of a window using the IceWM-remember-settings app. Select Select other Type Use the <b>Select other</b> option to select a different window/program to configure. Value What window configuration you want antiX to remember/forget? You can also delete this information unticking all options. workspace Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:49+0000
Last-Translator: Amigo, 2023
Language-Team: Spanish (https://www.transifex.com/anticapitalista/teams/10162/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 <b>Seleccionar un programa. Almacenar sus propiedades de ventana.</b> Agregar/Remover valores predeterminados de la ventana de IceWM Las entradas que se muestran a continuación son para la ventana <b>$appclass</b> ($appname).\n\nTodas las opciones marcadas se guardarán, todas las que no estén marcadas se eliminarán.\n\n Nota: el número de espacio de trabajo que se muestra es el espacio de trabajo actual de la ventana. \n 	No te preocupes si parece demasiado bajo.\n\n Geometría AYUDA Capa La próxima vez que inicie el programa, recordará las propiedades de la ventana guardadas la última vez. Guardar el <b>tamaño y posición</b>,<b>espacio de trabajo</b> y la <b>capa</b> de una ventana con la aplicación IceWM-recordar-configuración. Seleccionar Seleccionar otro Tipo Use la opción <b>Seleccionar otro</b> para seleccionar una ventana/programa diferente para configurar. Valor ¿Qué configuración de ventana desea que antiX recuerde/olvide? También puede eliminar esta información desmarcando todas las opciones. espacio de trabajo 